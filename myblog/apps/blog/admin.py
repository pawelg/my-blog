from django.contrib import admin
from django import forms
from blog.models import Category, Post
from ckeditor.widgets import CKEditorWidget

class PostAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Post

class PostAdmin(admin.ModelAdmin):
  form = PostAdminForm

admin.site.register(Category)
admin.site.register(Post, PostAdmin)
