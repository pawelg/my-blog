from django.conf.urls import patterns, url
from blog.models import Post
from blog import views
from django.conf.urls import handler404, handler400, handler403, handler500

urlpatterns = patterns('',
    url(r'^$', views.Index.as_view(), name = 'Index'),
    url(r'blog/', views.Blog_Archive.as_view(), name='Blog_Archive'),
    url(r'post/(?P<slug>[^\.]+)$', views.PostDetailView.as_view(), name='Post')
    )

handler404 = views.error404
handler500 = views.error500
handler403 = views.error403
handler400 = views.error400
