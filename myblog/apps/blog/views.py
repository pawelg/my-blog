from django.shortcuts import render, get_object_or_404
from blog.models import Category, Post
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView,DetailView
from django.shortcuts import render
from django.http import Http404


class Index(TemplateView):
  template_name = "blog/index.html"
  model = Post
  def get_context_data(self, **kwargs):
    try:
      context = super(Index, self).get_context_data(**kwargs)
      context['post'] =  Post.objects.latest('pub_date')
      return context
    except Post.DoesNotExist:
      return Http404



class Blog_Archive(ListView):
  template_name = 'blog/blog.html'
  context_object_name = "post_list"
  model = Post
  paginate_by = 5
  def get_context_data(self, **kwargs):
    try:
      context = super(Blog_Archive, self).get_context_data(**kwargs)
      return context
    except Post.DoesNotExist:
      return Http404

class PostDetailView(DetailView):
  model = Post
  template_name = "blog/post.html"
  slug_field = 'slug'
  object_name = "post"


def error404(request):

    # 1. Load models for this view
    #from idgsupply.models import My404Method

    # 2. Generate Content for this view
    template = loader.get_template('/errorpages/404.html')
    context = Context({
        'message': 'All: %s' % request,
        })

    # 3. Return Template for this view + Data
    return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=404)

def error500(request):

    # 1. Load models for this view
    #from idgsupply.models import My404Method

    # 2. Generate Content for this view
    template = loader.get_template('/errorpages/500.html')
    context = Context({
        'message': 'All: %s' % request,
        })

    # 3. Return Template for this view + Data
    return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=500)

def error403(request):

    # 1. Load models for this view
    #from idgsupply.models import My404Method

    # 2. Generate Content for this view
    template = loader.get_template('/errorpages/403.html')
    context = Context({
        'message': 'All: %s' % request,
        })

    # 3. Return Template for this view + Data
    return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=403)


def error400(request):

    # 1. Load models for this view
    #from idgsupply.models import My404Method

    # 2. Generate Content for this view
    template = loader.get_template('/errorpages/400.html')
    context = Context({
        'message': 'All: %s' % request,
        })

    # 3. Return Template for this view + Data
    return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=400)
