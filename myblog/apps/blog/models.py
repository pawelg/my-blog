from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.template.defaultfilters import slugify
from ckeditor.fields import RichTextField

class Category(models.Model):
  title = models.CharField(max_length = 100)
  def __str__(self):
    return self.title

@python_2_unicode_compatible
class Post(models.Model):

  title = models.CharField(max_length = 100)
  content = RichTextField()
  pub_date = models.DateTimeField()
  category = models.ForeignKey(Category)
  slug = models.SlugField()

  def __str__(self):
    return self.title

  def save(self, *args, **kwargs):
    self.slug = slugify(self.title)
    super(Post, self).save(*args, **kwargs)

  @models.permalink
  def get_absolute_url(self):
        return ('post', (), {
            'slug': self.slug,
        })

  class Meta:
    ordering = ['pub_date']
